<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiket', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('konser_id');
            $table->string('nomor_tiket',50);
            $table->string('nama_pengunjung',50);
            $table->string('nomor_hp',17);
            $table->string('alamat',50);
            $table->string('kota',50);
            $table->boolean('status_masuk');
            $table->timestamps();
            $table->foreign('konser_id')->references('id')->on('konser')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiket');
    }
}
