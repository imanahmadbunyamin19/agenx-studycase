@section('judul')
List Konser
@endsection

@extends('template.template')

@push('style')

@endpush

@section('content')

<div>
    
    <div class="row">
        
        @for ($i = 0; $i < $konser->count() ; $i++)
        <div class="col-4">
            <div class="col-md-12">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary ">
                    <h2 class="widget-user-username" style="background-color:black">
                        <strong> {{ $i+1 }} </strong>
                    </h2>
                    <h3 class="widget-user-username mt-1" style="color:black">
                        <strong> {{ $konser[$i]->nama_konser }}  </strong>
                    </h3>
                </div>

                <div >
                <img class="img-fluid img-thumbnail mt-2" src="{{ asset('images/konser.jpg') }}" alt="Konser">
                </div>

                <div>
                    <div class="row">
                        <div class="col-12 text-center mt-2">
                            <div class="col-12 text-center">
                                @guest
                                    {{-- Hanya Pengunjung yang dapat melakukan pemesanan tiket --}}
                                    <a href="/tiket/create" class="btn btn-primary btn-block mb-3">PESAN SEKARANG !</a>
                                @endguest
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        @endfor
    </div>
    
</div>
@endsection

@push('script')

@endpush
