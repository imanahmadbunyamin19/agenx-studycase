@section('judul')
Tambah Konser
@endsection

@extends('template.template')

@push('style')

@endpush

@section('content')

<div class="container-fluid">
  <div class="col-6">
    <form action="/konser" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nama Konser </label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Konser" value="{{old('name')}}" required autofocus>
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
    </form>
  </div>
</div>

@endsection

@push('script')
    
@endpush