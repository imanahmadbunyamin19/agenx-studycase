@section('judul')
Edit Konser
@endsection

@extends('template.template')

@push('style')

@endpush

@section('content')

<div>
  <div class="col-6">
    <form action="/konser/{{$konser->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama Konser </label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama konser" value="{{ $konser->nama_konser }}">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
    </form>
  </div>
</div>

@endsection



@push('script')

@endpush