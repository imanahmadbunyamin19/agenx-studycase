@section('judul')
Cek Tiket
@endsection

@extends('template.template')

@push('style')
    
@endpush

@section('content')

<div class="container-fluid">
    <form action="/cek/{tiket_id}" method="GET">
        <div class="form-group">
            <input type="text" class="form-control text-center" name="tiket_id" id="tiket_id" placeholder="Masukkan Tiket ID" value="" required autofocus>
            @error('tiket_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search">  Cek </i></button>
    </form>
</div>

@endsection

@push('script')

@endpush