@section('judul')
Detail Tiket 
@endsection

@extends('template.template')

@push('style')
    
@endpush


@section('content')

<div class="container-fluid mt-2">

    <a href="/cek" class="btn btn-primary"> Kembali Cek Tiket</a>
    
    <table class="table table-bordered mt-3">
        <tr>
            <td width="20%"><strong>Nama Pengunjung</strong></td>
            <td width="30%">{{ $tiket->nama_pengunjung }}</td>
            <td width="20%"><strong>Nomor HP</strong></td>
            <td width="30%">{{ $tiket->nomor_hp }}</td>
        </tr>
        <tr>
            <td width="20%"><strong>Alamat</strong></td>
            <td width="30%">{{ $tiket->alamat }}</td>
            <td width="20%"><strong>Kota</strong></td>
            <td width="30%">{{ $tiket->kota }}</td>
        </tr>
    </table>
</div>

@endsection



@push('script')

 
@endpush

