@section('judul')
Edit Pemesanan Tiket
@endsection

@extends('template.template')

@push('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endpush

@section('content')

<div class="container-fluid">
    <form action="/tiket/{{ $tiket->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="col-6">
            <div class="form-group">
                <label for="konser_id">Silahkan Pilih Konser</label>
                <select name="konser_id" class="form-control select2 " data-dropdown-css-class="select2-purple" style="width: 100%;" required autofocus>
                    <option value="{{ $tiket->konser_id }}">{{ $tiket->konser->nama_konser }}</option>
                    @foreach ($konser as $value)
                        <option value="{{ $value->id }}">{{ $value->nama_konser }}</option>
                    @endforeach
                </select>
                @error('konser_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_pengunjung">Nama </label>
                <input type="text" class="form-control" name="nama_pengunjung" id="nama_pengunjung" placeholder="Masukkan Nama Lengkap" value="{{$tiket->nama_pengunjung}}" required>
                @error('nama_pengunjung')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nomor_hp">Nomor HP </label>
                <input type="number" class="form-control" name="nomor_hp" id="nomor_hp" placeholder="Masukkan Nomor HP" value="{{$tiket->nomor_hp}}" required>
                @error('nomor_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat Lengkap </label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat Lengkap" value="{{$tiket->alamat}}" required>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="kota">Kota </label>
                <input type="text" class="form-control" name="kota" id="kota" placeholder="Masukkan Kota Asal" value="{{$tiket->kota}}" required>
                @error('kota')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </div>
    </form>
</div>

@endsection

@push('script')
    <!-- Select2 -->
    <script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2').select2({
        theme: 'bootstrap4'
        })
    })
    </script>
@endpush