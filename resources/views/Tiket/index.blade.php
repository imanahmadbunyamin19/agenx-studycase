@section('judul')
Data Tiket
@endsection

@extends('template.template')

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<div class="container-fluid">
    {{-- <a href="/konser/create" class="btn btn-primary mb-3 fa fa-plus-square"> Tambah </a> --}}
    <div class="card">
        <div class="card-body">
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="30%">Nama konser</th>
                    <th width="30%">Nomor Tiket</th>
                    <th width="30%">Nama Pengunjung</th>
                    <th width="15%">Nomor HP</th>
                    <th width="15%">Status Masuk</th>
                    <th width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($konser as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->konser->nama_konser}}</td>
                        <td>{{$value->nomor_tiket}}</td>
                        <td>{{$value->nama_pengunjung}}</td>
                        <td>{{$value->nomor_hp}}</td>
                        <td> 
                            @if ($value->status_masuk === 0)
                                Belum 
                            @else
                                Sudah
                            @endif 
                        </td>
                        <td>
                            <form action="/tiket/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/tiket/{{$value->id}}/edit" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Belum ada data ditemukan</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush
