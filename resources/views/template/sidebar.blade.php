<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="" alt="" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-light ml-4"><b>  Agen X</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      @auth
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
              <img src="{{asset('/images/user.png')}}" class="img-circle elevation-3" alt="User Image">
          </div>
          <div class="info">
              @auth
                <a href="/profil/{{ Auth::user()->id }}" class="d-block"> <strong> {{ Auth::user()->name }} </strong> </a>
              @endauth

              @guest
                <a href="#" class="d-block">Anda Belum Login</a>
              @endguest
          </div>
      </div>
      @endauth

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>List Konser</p>
          </a>
        </li>
        @auth
          <li class="nav-item">
            <a href="/konser" class="nav-link">
              <i class="nav-icon fas fa-music"></i>
              <p>Data Master Konser</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/tiket" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Data Pemesanan Tiket</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/cek" class="nav-link">
              <i class="nav-icon fas fa-search"></i>
              <p>Cek Tiket</p>
            </a>
          </li>
               
          <li class="nav-item bg-color:green">
            <a href="{{ route('logout') }}" class="nav-link"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa fa-times-circle"></i>
                <p>Logout</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
              @csrf 
            </form>
          </li>
        @endauth

        @guest
            
          <li class="nav-item">
            <a href="/login" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                Login
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
              @csrf 
            </form>
          </li>
        @endguest

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>