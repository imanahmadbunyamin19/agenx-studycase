<!DOCTYPE html>
<html lang="en">
<head><script nonce="34f29d01-4f1b-45c8-b91d-81fe04ab2899">(function(w,d){!function(a,e,t,r){a.zarazData=a.zarazData||{},a.zarazData.executed=[],a.zarazData.tracks=[],a.zaraz={deferred:[]},a.zaraz.track=(e,t)=>{for(key in a.zarazData.tracks.push(e),t)a.zarazData["z_"+key]=t[key]},a.zaraz._preSet=[],a.zaraz.set=(e,t,r)=>{a.zarazData["z_"+e]=t,a.zaraz._preSet.push([e,t,r])},a.addEventListener("DOMContentLoaded",(()=>{var t=e.getElementsByTagName(r)[0],z=e.createElement(r),n=e.getElementsByTagName("title")[0];n&&(a.zarazData.t=e.getElementsByTagName("title")[0].text),a.zarazData.w=a.screen.width,a.zarazData.h=a.screen.height,a.zarazData.j=a.innerHeight,a.zarazData.e=a.innerWidth,a.zarazData.l=a.location.href,a.zarazData.r=e.referrer,a.zarazData.k=a.screen.colorDepth,a.zarazData.n=e.characterSet,a.zarazData.o=(new Date).getTimezoneOffset(),z.defer=!0,z.src="/cdn-cgi/zaraz/s.js?z="+btoa(encodeURIComponent(JSON.stringify(a.zarazData))),t.parentNode.insertBefore(z,t)}))}(w,d,0,"script");})(window,document);</script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="icon" href="">
	<title>  @yield('judul') | AgenX</title>
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

	<link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">

	<link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css?v=3.2.0')}}">

	<link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css') }}">

	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	@stack('style')

</head>
<body class="hold-transition sidebar-mini">

	<div class="wrapper">
		@include('sweetalert::alert')
		
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">

			<ul class="navbar-nav">
				<li class="nav-item">
				  <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
				</li>
				<li class="nav-item d-none d-sm-inline-block">
				  <a href="/kontak" class="nav-link">Tentang</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<!-- Navbar Full Screen -->
				<li class="nav-item">
					<a class="nav-link" data-widget="fullscreen" href="#" role="button">
						<i class="fas fa-expand-arrows-alt"></i>
					</a>
				</li>
			</ul>

		</nav>


		@include('template.sidebar')

		<div class="content-wrapper">

			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>@yield('judul')</h1>
						</div>
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="/">Dashboard</a></li>
								<li class="breadcrumb-item active">@yield('judul')</li>
							</ol>
						</div>
					</div>
				</div>
			</section>

			<section class="content">

				@yield('content')
				
			</section>

		</div>

		<footer class="main-footer">
			<div class="float-right d-none d-sm-block">
				<b>Version</b> 1.0.1
			  </div>
			  <strong>Copyright &copy; 2022  <a href="#"> Agen X </strong></a>  All rights reserved.
		</footer>

		<aside class="control-sidebar control-sidebar-dark">

		</aside>

	</div>


	<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>

	<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

	<script src="{{asset('adminlte/dist/js/adminlte.min.js?v=3.2.0')}}"></script>

	<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>

	@stack('script')
</body>
</html>