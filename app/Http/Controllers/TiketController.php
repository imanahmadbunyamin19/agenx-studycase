<?php

namespace App\Http\Controllers;

use App\Models\Konser;
use App\Models\Tiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;


class TiketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $konser = Tiket::with('konser')->get();

        return view('Tiket.index', compact('konser'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $konser = Konser::all();

        return view('Tiket.create', compact('konser'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validations
        $request -> validate(
            [
                'konser_id' => 'required',
                'nama_pengunjung' => 'required',
                'nomor_hp' => 'required',
                'alamat' => 'required',
                'kota' => 'required',
            ],
            [
                'konser_id.required' => 'Konser Belum dipilih',
                'nama_pengunjung.required' => 'Nama Pengunjung Harus diisi',
                'nomor_hp.required' => 'Nomor HP Harus diisi',
                'alamat.required' => 'Alamat Harus diisi',
                'kota.required' => 'Kota Harus diisi',
            ]
        );

        // Insert New Data
        $order = Tiket::create(
            [
               'konser_id' => $request['konser_id'],
               'nama_pengunjung' => $request['nama_pengunjung'],
               'nomor_hp' => $request['nomor_hp'],
               'alamat' => $request['alamat'],
               'kota' => $request['kota'],
               'status_masuk' => 0,
            ]
        );

        Alert::success('Berhasil', 'Anda telah berhasil memesan tiket, Tunggu Kehadirannya yaa !');

        $customPaper = array(0,0,567.00,283.80);

        $pdf = PDF::loadview('tiket.print', compact('order'))->setPaper($customPaper, 'landscape');
        
        return $pdf->download('tiket.pdf');

        // return view('Konser.list');
        
    }

    public function edit($id)
    {
        $konser = Konser::all();
        $tiket = Tiket::findOrFail($id);
        return view('Tiket.edit', compact('konser','tiket'));
    }

    public function update(Request $request, $id)
    {
        $request -> validate(
            [
                'konser_id' => 'required',
                'nama_pengunjung' => 'required',
                'nomor_hp' => 'required',
                'alamat' => 'required',
                'kota' => 'required',
            ],
            [
                'konser_id.required' => 'Konser Belum dipilih',
                'nama_pengunjung.required' => 'Nama Pengunjung Harus diisi',
                'nomor_hp.required' => 'Nomor HP Harus diisi',
                'alamat.required' => 'Alamat Harus diisi',
                'kota.required' => 'Kota Harus diisi',
            ]
        );

        $tiket = Tiket::find($id);

        $tiket -> konser_id = $request-> konser_id;
        $tiket -> nama_pengunjung = $request-> nama_pengunjung;
        $tiket -> nomor_hp = $request-> nomor_hp;
        $tiket -> alamat = $request-> alamat;      
        $tiket -> kota = $request-> kota;      

        $tiket -> save();

        return redirect('/tiket');

    }


    // Index Cek Tiket
   public function CekIndex()
    {

        return view('Tiket.cek');

    }

    //Cek Tiket
    public function cektiket(Request $request)
    {
        
        $tiket = DB::table('tiket')->where('nomor_tiket', $request->tiket_id)->first();
        
        if ($tiket) {
            
            if ($tiket->status_masuk === 1){

                Alert::error('Gagal', 'Tiket tersebut sudah digunakan ');

                return redirect('/cek');    

            }else{

                // Jika ketemu dan belum masuk 

                $data = Tiket::find($tiket->id);

                $data -> status_masuk = 1;

                $data -> save();

                //redirect
                
                Alert::success('Berhasil', 'Tiket ditemukan');

                return view('tiket.show', compact('tiket'));

            }
        

        }else{

            Alert::error('Gagal', 'Tiket tersebut tidak terdaftar');

            return redirect('/cek');
        }

    }

    public function destroy($id){

        $tiket = Tiket::find($id);

        $tiket->delete();

        return redirect('/tiket');
    }

}
