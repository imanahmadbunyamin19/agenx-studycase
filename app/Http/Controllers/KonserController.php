<?php

namespace App\Http\Controllers;

use App\Models\Konser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KonserController extends Controller
{
    // Index Page
    public function index(Request $request)
    {
       
        return view('Konser.index',[
            "konser" => Konser::all()
        ]);
    }

    // Create 
    public function create()
    {
       return view('Konser.create');
    }

    // Save or store functions
    public function store(Request $request)
    {
        // Validations
        $request -> validate(
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Nama Konser Harus diisi',
            ]
        );

        // Insert New Data
        DB::table('konser')->insert(
            [
               'nama_konser' => $request['name'],
            ]
        );

        // Alert::success('Berhasil', 'Konser Baru sudah Ditambahkan');

        return redirect('/konser');
    }

    // to Edit Form
    public function edit($id)
    {
        $konser = Konser::findOrFail($id);

        return view('Konser.edit', compact('konser'));
    }

    // Update function
    public function update(Request $request, $id)
    {
        // Validations
        $request -> validate(
            [
               'name' => 'required',
            ],
            [
                'name.required' => 'Nama Konser harus diisi',
            ]
        );

        // Do Update
        $konser = Konser::find($id);

        $konser -> nama_konser = $request-> name;

        $konser -> save();

        // Alert::success('Berhasil', 'Data Konser Berhasil Dirubah');

        return redirect('/konser');
    }

    // delete or destroy
    public function destroy($id)
    {
        $konser = Konser::find($id);
        $konser->delete();

        // Alert::success('Berhasil', 'Data Konser Berhasil Dihapus');

        return redirect('/konser');
    }

    // list konser (Halaman utama untuk pengunjung)
    public function list()
    {
        return view('Konser.list',[
            "konser" => Konser::all()
        ]);
    }
}
