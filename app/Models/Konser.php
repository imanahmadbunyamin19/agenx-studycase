<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konser extends Model
{
    protected $table = 'konser';
    protected $fillable = ['nama_konser'];

    public function tiket(){
        return $this->hasMany(Tiket::class);
    }
}
