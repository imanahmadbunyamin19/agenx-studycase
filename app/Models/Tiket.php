<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Alfa6661\AutoNumber\AutoNumberTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Konser;

class Tiket extends Model
{
    protected $table = 'tiket';
    protected $fillable = ['konser_id', 'nomor_tiket', 'nama_pengunjung', 'nomor_hp', 'alamat', 'kota', 'status_masuk'];

    public function konser(){
        return $this->belongsTo(Konser::class);
    }

    use AutoNumberTrait;
    public function getAutoNumberOptions()
    {
        return [
            'nomor_tiket' => [
                'format' => function () {
                    return 'AGEN-X' . date('Ymd') . $this->konser_id . '?'; 
                },

                'length' => 5,
            ]
    ];
}
}
