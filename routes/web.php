<?php

use App\Http\Controllers\KonserController;
use App\Http\Controllers\TiketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ->middleware('auth');
//Data Konser
Route::get('/konser', [KonserController::class,'index'])->middleware('auth');
Route::get('/konser/create', [KonserController::class,'create'])->middleware('auth');
Route::post('/konser', [KonserController::class,'store'])->middleware('auth');
Route::get('/konser/{konser_id}/edit', [KonserController::class,'edit'])->middleware('auth');
Route::put('/konser/{konser_id}', [KonserController::class,'update'])->middleware('auth');
Route::delete('/konser/{konser_id}', [KonserController::class,'destroy'])->middleware('auth');


Route::get('/', [KonserController::class,'list']);


// Pemesanan Tiket
Route::get('/tiket', [TiketController::class,'index'])->middleware('auth');
Route::get('/tiket/create', [TiketController::class,'create'])->middleware('guest');
Route::post('/tiket', [TiketController::class,'store'])->middleware('guest');

Route::get('/tiket/{tiket_id}/edit', [TiketController::class,'edit'])->middleware('auth');
Route::put('/tiket/{tiket_id}', [TiketController::class,'update'])->middleware('auth');

//print tiket
Route::get('/tiket/{tiket_id}', [TiketController::class,'print'])->middleware('guest');


// Cek Tiket
Route::get('/cek', [TiketController::class,'cekIndex'])->middleware('auth');
Route::get('/cek/{tiket_id}', [TiketController::class,'cekTiket'])->middleware('auth');


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
